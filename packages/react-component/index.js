import { Route, Routes } from "react-router-dom";
import { useEffect, useReducer, useState } from "react";
import { SongDetail } from "./pages/songDetail";
import { Songs } from "./pages/songs";
import { Navbar } from "./pages/navbar";
import { Drawing } from "./pages/drawing";
import { SongAlter } from "./pages/songAlter";
import { SongReducer } from "./reducers/songReducer";
import { SongApiService } from "./services/songApiService";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

export default function () {
  const [songsState, dispatch] = useReducer(SongReducer, {
    songs: [],
    error: null,
  })

  const [animationState, setAnimationState] = useState(true);

  useEffect(() => {
    SongApiService.get().then((songs) => {
      dispatch({ type: 'initialize', payload: songs })
    })
  }, [])

  const drawing = () => {
    if(animationState)
      return <Drawing/>
    return <></>
  }

  return (
    <div className="appBackground">
      <Navbar animationState={animationState} setAnimationState={setAnimationState} />
      <Routes>
        <Route path={`/`} element={<Songs songsState={songsState} dispatch={dispatch} />} />
        <Route path={`/:uuid`} element={<SongDetail songsState={songsState} />} />
        <Route path={`/add`} element={<SongAlter dispatch={dispatch} />} />
        <Route path={`/:uuid/edit`} element={<SongAlter songsState={songsState} dispatch={dispatch} />} />
      </Routes>
      {drawing()}
      <ToastContainer />
    </div>
  )
};



