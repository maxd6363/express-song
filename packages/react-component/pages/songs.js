import "../style/songs.css"
import { Song } from "./song";
import { Link } from "react-router-dom";
import plus from "../assets/icons-plus.svg"

export const Songs = ({ songsState, dispatch }) => {
  const songs = songsState.songs
  const grid = songs?.map((song, index) => {
    return (
      <Song key={index} song={song} dispatch={dispatch}></Song>)
  })

  return (
    <div id="songsRoot">
      <div className="text-center">
        <div className="main d-inline-flex flex-wrap pt-2">
          {grid}
        </div>
      </div>
      <Link id="addButton" to={"/song/add"}>
        <img alt="plus" src={plus}></img>
      </Link>
    </div>
  )
}
