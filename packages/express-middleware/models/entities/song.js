const Mood = require('../mood')

class Song {
    uuid = ""
    name = ""
    duration = 0
    mood = ""
    user = ""

    constructor(props) {
        this.uuid = props.uuid
        this.name = props.name
        this.duration = props.duration
        this.mood = props.mood
        this.user = props.user
    }

    valid() {
        return this.name &&
            this.duration &&
            this.mood &&
            typeof this.duration === 'number' &&
            this.mood in Mood
    }
}

module.exports = Song