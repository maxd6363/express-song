class SongDTO {
    uuid = ""
    name = ""
    duration = 0
    mood = ""

    constructor(props) {
        this.uuid = props.uuid
        this.name = props.name
        this.duration = props.duration
        this.mood = props.mood
    }
}

module.exports = SongDTO